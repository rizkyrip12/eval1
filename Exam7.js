function Exam0807(n = 1){
    var arr2d = GetArray2D(2, n);
    var a = 1;
    var i = 4;

    for(let row = 0; row < 2; row++){
        for(let col = 0; col < n; col++){
            if(row == 0 && col == 1){
                arr2d[row][col] =  a;
                a += 2;
            }else if(col == i){
                arr2d[row][i] = -1*a;
                i += 4;
            }else{
                a -= 2;
                arr2d[row][col] = a;
            }
        }
    }
    return print(arr2d);
}