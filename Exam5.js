function Exam0805(angka = [1]) {
    var hasil = [];

    while (true) {
        for (let i = 1; i < angka.length; i++) {
            if (angka[i - 1] > angka[i]) {
                var temp = angka[i - 1];
                angka[i - 1] = angka[i];
                angka[i] = temp;
            }
        }
        var sorted = true;
        for (let i = 1; i < angka.length; i++) {
            if (angka[i - 1] > angka[i]) {
                sorted = false;
            }
        }
        if (sorted) {
            break;
        }


    }
    //MEAN
    var sum = 0;
    for (let idx = 0; idx < angka.length; idx++) {
        sum = angka[idx] + sum;
    }
    hasil[0] = sum / angka.length;

    //MEDIAN
    if (angka.length % 2 == 1) {
        var m = (angka.length + 1) / 2;
        hasil[1] = angka[m - 1];
    } else {
        var m = ((angka.length) / 2);

        hasil[1] = (angka[m] + angka[m - 1]) / 2;
    }

    //MODUS
    var a = [];
    var max = 0;
    var nilai = 0;

    for (let idx = 0; idx < angka.length; idx++) {
        var sum = 0;

        for (let a = 0; a < angka.length; a++) {
            if (angka[idx] == angka[a]) {
                sum++;
            }
            if (sum > max) {
                max = sum;
                nilai = angka[idx];
            }
        }

    }
    hasil[2] = nilai;

    //return
    return hasil;

}